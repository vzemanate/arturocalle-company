﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logic
{
    public class Buy
    {
        private double total;
        private int value;
        private double payment;
        private double discount;

        
        public double Total { get => total; set => total = value; }
        public int Value { get => value; set => this.value = value; }
        
        public double Payment { get => payment; set => payment = value; }
        public double Discount { get => discount; set => discount = value; }

        public double totalPayment()
        {
            int valueSuit = 125000;
          
            if (value <= valueSuit)
            {
                discount = value * 0.1;
                payment = value + discount;
               
            }
       
        else
            {
                discount = value * 0.35;
                payment = value + discount;
            }
            
            return payment;
        }
        

        }

    }
